close all; clear all;

I = imread('IMG_0615.JPG');
opts.nCorners = 1000;
opts.hW = 32;
opts.delta=5;
opts.u=10;
opts.cThresh=0.005;
opts.iRC = 2;
opts.dGamma = 0.01;
opts.maxiter=2500 ;
opts.convergedThresh = 10^-6;
opts.NComparison = 2;
opts.makePlots = true;opts.displayText = true;
[C,d,p] = lca_corners_localDisp_globalParams(I,opts);


%%
% M = mi(I(:,:,1),I(:,:,2));
CC01 = corr2(I(T:end-T,T:end-T,1),I(T:end-T,T:end-T,2));
CC03 = corr2(I(T:end-T,T:end-T,3),I(T:end-T,T:end-T,2));
T = 10;
p0 = [size(I,2)/2 size(I,1)/2 1]';
I1 = induceLCA(I,p{1},p0,1);
I1 = induceLCA(I1,p{2},p0,3);
CC1 = corr2(I1(T:end-T,T:end-T,1),I(T:end-T,T:end-T,2));
CC3 = corr2(I1(T:end-T,T:end-T,3),I(T:end-T,T:end-T,2));
% [aa,bb,cc] = ent(I(:,:,1),I(:,:,2));
