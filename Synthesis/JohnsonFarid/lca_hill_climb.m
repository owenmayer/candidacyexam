% close all; clear all;
% I = imread('IMG_0615.JPG');
% init = [2304 1728 1];
% delta = [50 50 0.00025];
% smax = [size(I,2) size(I,1) 1.001];
% smin = [0 0 0.999];
% T = 10;
% iC = 1;
% iR = 2;

function [bestsoln,best,BESTSOLN,BEST,list,score] = lca_hill_climb(I,init,DELTA,T,iC,iR)

smax = [size(I,2) size(I,1) 1.01];
smin = [0 0 0.99];

for ii = 1:size(DELTA,1);
delta = DELTA(ii,:); 
fprintf('Granularity %d of %d... ',ii,size(DELTA,1));
[bestsoln,best,BESTSOLN{ii},BEST{ii},list{ii},score{ii}] = climb_hill(I,init,delta,smax,smin,T,iC,iR);
fprintf('Done.\n');
smin = bestsoln - 2*delta;
smax = bestsoln + 2*delta;
init = bestsoln;


end


function [bestsoln,best,BESTSOLN,BEST,list,score] = climb_hill(I,init,delta,smax,smin,T,iC,iR)

%% CREATE NEIGHBORHOOD
delta2 = vertcat(-delta,[0,0,0],delta);
[X,Y,A] = meshgrid(delta2(:,1), delta2(:,2),delta2(:,3));
X = reshape(X,[],1); Y = reshape(Y,[],1); A = reshape(A,[],1);
DELTA = horzcat(X,Y,A);


%% FIRST ITERATION
p0 = [size(I,2)/2 size(I,1)/2 1]';
I1 = induceLCA(I,init',p0,iC);
best = corr2(I1(T:end-T,T:end-T,iC),I1(T:end-T,T:end-T,iR));
BEST = best;
score = best;
bestsoln = init;
BESTSOLN = bestsoln;
list = init;
iter = 1;
%%
prevsoln = [0 0 0];
%DECIDE ON SOLUTIONS TO TRY
while ~all(prevsoln==bestsoln);
    solntry = repmat(bestsoln,size(DELTA,1),1) + DELTA; %check the neighborhood
    solntry = setdiff(solntry,list,'rows'); %remove ones already tried
    solntry = solntry(~sum(solntry < repmat(smin,size(solntry,1),1),2),:); %remove ones below min search space
    solntry = solntry(~sum(solntry > repmat(smax,size(solntry,1),1),2),:); %remove ones above max search space
    prevsoln = bestsoln; %save the previous solution
    for ii = 1:size(solntry,1); %try each solution
%         fprintf('%d of %d\n',ii,size(solntry,1))
        iter = iter + 1;
        I1 = induceLCA(I,solntry(ii,:)',p0,iC); %transform color plane
        score(iter) = corr2(I1(T:end-T,T:end-T,iC),I1(T:end-T,T:end-T,iR)); %check the sim. score
        
        if score(iter)>best; %if its the best so far
            best = score(iter); %save it!
            bestsoln = solntry(ii,:);
%             fprintf('New best of %2.5f at [%d %d %1.5f]  \n',best,bestsoln);
        end
    end
    BEST = vertcat(BEST,best);
    BESTSOLN = vertcat(BESTSOLN,bestsoln);
    
    
    list = vertcat(list,solntry); %append what we tried to the list
end

