close all; clear all;
I = imread('IMG_0615.JPG');


init = [2304 1728 1];
DELTA = [50 50 0.00020;
    10 10 0.0001;
    2 2 0.0000333];

% smax = [size(I,2) size(I,1) 1.001];
% smin = [0 0 0.999];
T = 10;
iC = 1;
iR = 2;

[bestsoln,best,BESTSOLN,BEST,list,score] = lca_hill_climb(I,init,DELTA,T,iC,iR);


%%
figure;
for ii = 1:size(BESTSOLN,2)
% BESTSOLN = vertcat(BESTSOLN1,BESTSOLN2,BESTSOLN3);
plot3(BESTSOLN{ii}(:,1),BESTSOLN{ii}(:,2),BESTSOLN{ii}(:,3),'o-','LineWidth',2);
hold on;
end
% plot3(BESTSOLN{1}(1,1),BESTSOLN{1}(1,2),BESTSOLN{1}(1,3),'o','LineWidth',2);
% plot3(BESTSOLN{end}(end,1),BESTSOLN{end}(end,2),BESTSOLN{end}(end,3),'x','LineWidth',2);
grid on;
xlabel('x_0');ylabel('y_0');zlabel('\alpha');
set(gca,'FontSize',12)