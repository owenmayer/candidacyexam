close all; clear all;

I = imread('007.JPG');

BoxW = 128;
pctOvrlp = 0.5;
Ovrlp = round(BoxW*pctOvrlp);

init = [2304 1728 1];
DELTA = [50 50 0.00020;
    10 10 0.0001;
    2 2 0.000025];
% p0 = [2522 1822 1.0007]';

% smax = [size(I,2) size(I,1) 1.001];
% smin = [0 0 0.999];
T = 5;
iC = 3;
iR = 2;

globalSoln = lca_hill_climb(I,init,DELTA,T,iC,iR);
p0 = globalSoln';
%%

%GET BOX INDICES
[BIND, BJND] = imoverlappingboxes(I,BoxW,BoxW,Ovrlp,Ovrlp);

%%

muG = zeros(size(BIND,1),1);
nbar = 0;
for ii = 1:size(BJND,1);
    subI = I(BJND(ii,:),BIND(ii,:),:);
    G = imgradient(subI(:,:,2),'central');
    muG(ii) = mean(G(:));
    nbar = cmdWaitBar(ii/size(BJND,1),'Calculating Gradients',nbar);
end


%%
figure;
imshow(I); hold on;
gThresh = 3.2;
for ii = 1:size(BJND,1);
    if muG(ii) >= gThresh
        rectangle('Position',[BIND(ii,1) BJND(ii,1) BoxW BoxW],'EdgeColor','g')
    end
end

%%


% for ii = 1:size(BJND,1);
%     if muG(ii) >= gThresh
%         rectangle('Position',[BIND(ii,1) BJND(ii,1) BoxW BoxW],'EdgeColor','g');
%
localSoln = zeros(size(BJND,1),3);
for ii = 1:size(BJND,1);
    if muG(ii) >= gThresh
        fprintf('%d of %d\n',ii,size(BJND,1));
        subI = I(BJND(ii,:),BIND(ii,:),:);
        init = p0 - [BIND(ii,1) BJND(ii,1) 0]';
        blocksoln = lca_hill_climb(subI,init',DELTA,T,iC,iR);
        localSoln(ii,:) = blocksoln + [BIND(ii,1) BJND(ii,1) 0];
    end
end


%%
R = [BIND(muG >= gThresh,1) BJND(muG >= gThresh,1)];
[~,globD] = JohnsonFaridLCAmodel(R,p0);
locD = [];
for ii = 1:size(BJND,1);
    if muG(ii) >= gThresh
        [~,tempD] = JohnsonFaridLCAmodel([BIND(ii,1) BJND(ii,1)],localSoln(ii,:)');
        locD = vertcat(locD,tempD);
    end
    
end
    
figure;
plot_dLCA_vectorField(R,globD,-200,'r');
hold on;
plot_dLCA_vectorField(R,locD,-200,'b');
axis ij;
