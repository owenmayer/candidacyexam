figName = 'cpfd_AllToSAN_23Sep2015';

H = open([figName '.fig']); %load fig

ylabel({' ';'P_D'}); %so ylabel doesn't get cutoff
boldify; %make it look nice
tightfig; %trim the edges

print(figName,'-dpdf','-r600'); %print high-res