close all; clearvars;
I(:,:,1) = imread('rice.png');
I(:,:,2) = imread('rice.png');
I(:,:,3) = imread('rice.png');

I1 = induceLCA(I,[128 128 1]',[128 128 1.025]',1);
I2 = induceLCA(I,[128 128 1]',[128 128 .975]',3);

% I = I; 
% I(:,:,3) = I2(:,:,3); 
I(:,:,1) = I1(:,:,1);

imshow(I(5:end-5,5:end-5,:))
tightfig;
% print('lca_example_large','-dpdf','-r600')



imshow(I(180:250,180:250,:))
% tightfig;
print('lca_example_block','-dpdf','-r600')