\section{A SIFT-Based Forensic Method for Copy–Move Attack Detection and Transformation Recovery}

A third paper, titled \textit{A SIFT-Based Forensic Method for Copy–Move Attack Detection and Transformation Recovery} by Amerini et al.~\cite{amerini2011sift}, presents a technique for determining the authenticity of images against copy-move attacks, where content has been cut and pasted with the same image. To do this, the authors utilize the scale invariant feature transform (SIFT) to correlate content in the original and duplicated image regions. SIFT features are invariant to geometric transformations, including scaling and rotation, as well as other operations such as blurring and compression. These are desirable properties for copy-move detection.

%Furthermore, many times the duplicated region will be geometrically transformed. For example,
Often, the duplicated content will need to match the size and orientation of scene the that it is being placed in. As a result, the forger will scale and rotate the duplicated content to appropriately match it with the scene. The authors propose an algorithm to determine the geometric transform between the original and duplicate region, provided that the transformation is affine.

To detect duplicated regions, the authors propose an algorithm as follows. First, the SIFT algorithm is applied to the image, which selects keypoints and assigns local descriptors to each keypoint. The local descriptors of the duplicated keypoints are similar to the descriptors of the original keypoints. Next, the distance between each keypoint descriptor is calculated between, and matches are made between corresponding keypoints. Then, a clustering algorithm is employed to finally make a decision as to whether the image has been tampered. 

If it has been determined that the image has undergone a copy-move attack, the authors employ a method to determine the geometric transformation between the original and duplicate regions. To do this, they employ a RANSAC algorithm that iteratively decides tries possible transformations, and selects the one that best fits the most keypoints.

%Brief key findings... 
%The authors also show that this method can also be used to determine cut-paste attacks in the scenario where the image containing the original sourced content is available.

\subsection{The scale invariant feature transform (SIFT)}

To detect duplicated image regions, feature descriptors of image content are built that are the same in both the original and duplicated regions. To do this, the scale invariant feature transform (SIFT) algorithm is employed \cite{lowe2004distinctive}, which builds features that are invariant to geometric transformation, as well as gamma corrections, blurring and compression. 

The SIFT algorithm contains three main components. First, keypoints are determined. Second, a canonical orientation is assigned to each keypoint. Then finally, a descriptor for the keypoint is determined which will match those of corresponding keypoints in duplicated content.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/sift_dog1}
        \caption{Keypoint localization via DoG}
        \label{sfig:sift_dog}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth,trim={4cm 8.5cm 4cm 8.5cm},clip]{figs/sift_basic_4.pdf}
        \caption{SIFT feature descriptors}
        \label{sfig:sift_feature_example}
    \end{subfigure}
    \caption{Visualizations of key parts of the SIFT algorithm.}\label{fig:Sift_DoG_Feat}
\end{figure}


\subsubsection{Keypoint localization}
To determine keypoint locations, a Difference of Gaussian (DoG) method is employed. The method blurs the image $I(x,y)$ with a Gaussian blur kernel $G(x,y,\sigma)$ that uses the standard deviation $\sigma$ as a scale factor. The blurred version of the image  
%
\begin{equation}
L(x,y,\sigma) = G(x,y,\sigma)\star I(x,y)
\end{equation}
is calculated using convolution. Then, the difference at different blurring scales is taken 
\begin{equation}
D(x,y,\sigma) = L(x,y,k\sigma) - L(x,y,\sigma)
\end{equation}
for some factor $k$. Since Gaussian blurring is effectively a low-pass filtering operation, subtracting an image by its blurred image $L(x,y,\sigma)$ removes low-frequency content. This creates an image of just high frequency content (i.e. corners, edges). Subtracting two blurred images from each other selects content of a certain frequency ranges (i.e. corners, edges of certain scales). 

To select keypoints, the 3x3 spatial neighborhood of $D(x,y,\sigma)$ is inspected for maximums/minimums, as well as the corresponding scale neighborhood of $D(x,y,k^{-1}\sigma)$ and $D(x,y,k^{1}\sigma)$. If $D(x,y,\sigma)$ is a minimum or maximum of these neighborhood, it is selected as a keypoint. A diagram of the neighborhood selection is made clear by Fig. \ref{sfig:sift_dog}, which shows the three DoG planes of the neighborhood. This method selects object ``blobs" of scale (size) according to $\sigma$.

\subsubsection{Orientation Assignment}
Next, an orientation is assigned to each keypoint. An orientation is necessary since it is possible that the duplicated region has undergone a rotation, and thus the feature descriptors need to be referenced to a common orientation.
%
This is done by determining the gradient direction of the keypoint, as well as the gradient direction of its neighbors. The gradient direction $\theta(x,y)$ at point (x,y) is defined by
\begin{equation}
\theta(x,y) = \tan^{-1}\left(
\frac{L(x,y+1) - L(x,y-1)}{L(x+1,y) - L(x-1,y)}
\right)
\end{equation}
where $L(x,y)$ is the gaussian blurred image at the keypoint scale. A directional histogram is then formed using the gradient directions of the keypoint spatial neighborhood, with bins added according to the magnitude of the gradient $m(x,y)$, as defined by the following:
%
\begin{equation}
m(x,y) = \sqrt{\left(L(x+1,y) - L(x-1,y) \right)^2 +
\left(L(x,y+1) - L(x,y-1) \right)^2}
\end{equation}

A canonical orientation $o$ is assigned to the orientation that has the highest histogram count, indicating a dominant direction for the keypoint.

\subsubsection{Local Descriptor Features}
Local descriptors are then generated that are used to match keypoints in duplicated content to the keypoints in the original region. The local descriptor $\mathbf{f}$, contains 128 elements comprised of angular histogram bins. A 16x16 neighborhood of the blurred image $L(x,y,\sigma)$ about keypoint location $(x,y)$ is subdivided into 16 non-overlapping 4x4 windows, oriented about the keypoint orientation. An 8-bin histogram of orientations $\theta(x,y)$ for each pixel location $(x,y)$ in the 4x4 window is made. The bin counts are weighted by the gradient magnitudes. The full feature vector $\mathbf{f}$ contains the 128 elements, comprised of 8-bin histogram counts from 16 4x4 windows. The local descriptor $\mathbf{f}$ provides a relatively low dimensional description of the image content surrounding a keypoint, where the description is in the form of angular histogram counts, weighted by gradient magnitude.

Fig. \ref{sfig:sift_feature_example} provides a depiction of the keypoint features. The yellow circle represents the keypoint location, and the green grid shows the 16 4x4 pixel windows orientated to the keypoint orientation. Inside each grid cell shows an angular histogram, where the bar length represents the weighted bin count. The grid is scaled to represent the keypoint scale.

\subsection{Forgery Detection}

From the SIFT algorithm, a set of N keypoints are generated in the image, where the ith keypoint is characterized by $\mathbf{x}_i = \left\lbrace x,y,\sigma,o,\mathbf{f} \right\rbrace$, where (x,y) is the keypoint location, $\sigma$ is its scale, $o$ is its orientation, and $\mathbf{f}$ is the local descriptor vector containing orientation histograms. The local descriptors $\mathbf{f}$ of keypoints in duplicated content are similar to the local descriptors in the original region. The authors propose an algorithm that finds clusters of keypoints that have local descriptors correlated to the local descriptors to one or more other clusters. If at least two such clusters exist, the image is deemed to have undergone a copy-move attack.

\subsubsection{Keypoint Matching}

For each keypoint, the Euclidean distance between its descriptor $\mathbf{f}$ and that of each other keypoint. These distances are then sorted into a vector of ascending distances 
$\mathbf{D} = \left\lbrace d_1,d_2,\ldots,d_{n-1} \right\rbrace$.

The first k keypoints are considered a match at the first instance where $\tfrac{d_k}{d_{k+1}} < T$ with the threshold $T$ set to 0.5. For example, if there are 2 corresponding keypoints (i.e. the region was duplicated twice), $d_1$ and $d_2$ will be small with $d_3$ through $d_{n-1}$ larger. The ratio $\tfrac{d_1}{d_{2}}$ will be near unity, since both $d_1$ and $d_2$ match and will have similar distances. However, $\tfrac{d_2}{d_{3}}$ will be small since $d_2$ is small and $d_3$ is much larger. The algorithm stops iterating here, and keypoints corresponding to $d_1$ and $d_2$ are considered a match to the keypoint being tested.

This procedure is repeated for all keypoints, and those keypoints that have a corresponding matches are added to a list of potential duplicates.

\subsubsection{Clustering and Forgery Detection}

To find clusters of keypoints, the authors employ an agglomerative clustering method performed on keypoint spatial locations. The agglomerative method starts by considering each keypoint as a cluster with singular membership. At each step of the algorithm, clusters with the smallest distance are joined to form a new cluster. Cluster to cluster distances are computed using one of a variety of methods including single, centroid and Ward's linkages. Clusters are iteratively joined, forming a hierarchical linkage tree until joining the next closest cluster exceeds a linkage threshold $T_h$. 

At the end of the agglomerative clustering operation, if there exists at least two clusters with at least 3 keypoints, the image is considered tampered.



\subsection{Transformation Recovery}

If the image has undergone a copy-move attack, and the corresponding duplicate and original keypoints have been identified, then the geometric transformation between the original and duplicate regions is determined. The authors consider the linear transformation between an original point $(x,y)$ to duplicate point $(x',y')$ via transformation matrix $\mathbf{H}$, such that

\begin{equation}
\begin{pmatrix}
x' \\ y' \\ 1
\end{pmatrix}
= \mathbf{H}
\begin{pmatrix}
x \\ y \\ 1
\end{pmatrix}.
\end{equation}
%
The transformation matrix $\mathbf{H}$ is defined as an affine transformation $\mathbf{A}$ and translation $\mathbf{t}$, with
\begin{equation}
\mathbf{H} = \begin{bmatrix}
\mathbf{A} & \mathbf{t} \\
\mathbf{0}^T & 1
\end{bmatrix}, \qquad where \qquad 
%A = R(\theta)(R(-\Phi)SR(+\Phi))
\mathbf{A} = \begin{bmatrix} a_{11} & a_{12} \\
a_{21} & a_{22}
\end{bmatrix}.
\end{equation}
The matrix $\mathbf{A}$ describes the family of affine transformations, which include scaling, rotations and shearing. 

\subsubsection{RANdom SAmple Consensus (RANSAC)}

There may exist some false matches between corresponding keypoint clusters. Attempting to recover the transformation matrix $\mathbf{H}$ (via a least squares estimate) in such a scenario would introduce significant error. As a result, the authors use a more robust method called the RANdom SAmple Consensus (RANSAC). 

The RANSAC method estimates the transformation from just 3 matching keypoints (the minimum needed to calculate $\mathbf{H}$). Then the transformation is applied to remaining original keypoints, producing estimates of their locations in the duplicate region. Error is calculated between the estimated duplicate keypoint locations and the actual duplicate keypoint location. Those estimate locations with error less than a certain threshold are deemed ``inliers." This method is repeated for a fixed number of times, and the transformation estimate that maximizes the number of inliers is chosen. By repeating this method enough times, there will be a high probability that at least one set of the 3 matching keypoints will not contain an erroneous match and thus correctly estimate the true transformation.  