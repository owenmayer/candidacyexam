\section{Digital Image Forgery Detection Based on Lens and Sensor Aberration}

Authors Yerushalmy and Hel-Or, in their paper titled \textit{Digital Image Forgery Detection Based on Lens and Sensor Aberration} \cite{yerushalmy2011digital}, present an algorithm for detecting cut-paste attacked images. Like the work by Johnson and Farid~\cite{johnson2006exposing}, this work uses chromatic aberrations as an inherent imaging fingerprint to expose forgeries. They do this by determining the tampered regions where chromatic aberrations are inconsistent from the rest of the image. This paper uses a different set of chromatic aberrations that include sensor induced distortions called \textit{purple fringing aberrations} (PFA). 


\subsection{Purple Fringing Aberrations (PFA)}

Purple fringing aberrations (PFA) manifest as blue-purple halos around the distal side of bright objects, relative to the image center, and around the proximal side of dark objects. PFA are attributed to various sensor effects including electron overflow into neighboring pixels, sensitivity to infrared radiation, and light impingement onto neighboring cells due to sensor micro-lenses. PFA is also compounded by certain lateral chromatic aberrations that occur as purple/yellow fringes.

Fig. \ref{sfig:PFA_diagram} shows the PFA effect in a cartoon image. The bright ring has purple fringe on its distal (further) side relative to image center (represented by the plus sign). There is a yellow ring on the distal side attributed to LCA. Fig. \ref{sfig:PFA_example} shows an example of how PFA manifests in a real-world image. The furthest right inset is a zoom-in of the top-left of the image, where the blue-purple fringe is clearly visible.


\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/pfa_cartoon.png}
        \caption{PFA Diagram}
        \label{sfig:PFA_diagram}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/pfa_example.png}
        \caption{Example of PFA in an Image}
        \label{sfig:PFA_example}
    \end{subfigure}
    \caption{A diagram (a) and image (b) showing how PFA manifests in images. Blue-purple fringes appear on the distal side, relative to the image center, of bright objects.} \label{fig:PFA_diagram}
\end{figure}

The authors note three primary properties of purple fringing aberrations that are used for forgery detection. First, PFA manifests as a purple fringe on the distal side of bright object, and on the proximal side of dark object with respect to the image center. Second, the strength of the PFA effect increases with an increase in contrast (difference in pixel values) across an object edge. Finally, the PFA effect is more prominent near the image periphery.

\subsection{Forgery Detection Algorithm}
To detect image forgeries, the authors propose an algorithm that locates the image center as predicted by observed PFA events. They do this using the direction of observed PFA events combined with their measures of reliability. Then, image regions that do not point towards estimated the image center are determined to have been implanted through tampering. Their algorithm is summarized as follows:

\begin{enumerate}
\item Identify where PFA events occur in the image
\item Determine a direction for each PFA event
\item Assign a reliability according to PFA strength, edge contrast, and distance from the image center
\item Robustly determine the image center $\mathbf{X}_0 = (x_0,y_0)$
\item Identify PFA events whose direction do not agree with the image center $\mathbf{X}_0$. If such events exist, mark those locations as tampered.
\end{enumerate}

\subsubsection{Identifying PFA Events}
PFA events occur at object edges where pixel intensity changes from low-to-high or high-to-low. To locate these pixels, a Canny edge detector is employed~\cite{canny1986computational}. Then, each edge pixel is inspected to determine if there is a PFA event there. This is done by taking a chroma profile of pixel values perpendicular to the edge. The chroma pofile uses the xyY color space. The chromaticity (x-y) plane (at a fixed luminance Y) of the xyY color space is shown in Fig. \ref{sfig:xyY_space}. At edges where there is no PFA event, it is expected that the profile traverses the chromaticity plane in a straight line. In edges where there is a PFA event, the chromaticity profile across the edge will deviate towards the blue-purple hue. 

Fig. \ref{sfig:xyY_profiles} shows examples of both cases. On the left is an example of a non-PFA profile, where the chromaticity profile is linear. The example on the right shows an example of a PFA event, where the chromaticity profile deviates towards the blue-purple chroma as denoted by the purple vector. The amount of deviation is described as PFA ``strength," which is formally described in Sec. \ref{sssec:pfa_strength}. Pixel locations with PFA strength above a threshold are declared PFA events.


\subsubsection{Determining PFA Direction}
Since PFA events occur on the distal side of bright objects, the intensity gradient of an image edge is used to imply the direction towards the image center. A unit vector $\vec{N}$ is determined that points perpendicular to the object edge, in the direction of increasing intensity.

\subsubsection{Determining PFA Strength}

PFA strength is determined by the deviation of chromaticity values across an image edge towards the blue-purple chroma. To do this, a line-segment is drawn between the starting and ending chroma, representing the expected profile in the case of no PFA. This is shown in Fig. \ref{sfig:xyY_profiles}, where the red square and black dot represent the starting and ending chroma, and the gray line represents the expected transition profile. Then the value of chromaticity along the actual profile that is the greatest distance from the expected transistion line segment is determined. This shown by the yellow diamond in Fig. \ref{sfig:xyY_profiles}. A vector is drawn from the expected segment mid-point to the most distant point of the sequence. To give scalar value of PFA ``strength" this vector is then projected onto a vector that points toward the blue-purple direction, which is defined from the mid-segment point to the x=0.2 y=0.1 (450nm) point. This projection results in a value $s$ that provides a measure of PFA strength.


\label{sssec:pfa_strength}
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figs/xyY_colorspace}
        \caption{xyY chromaticity}
        \label{sfig:xyY_space}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.7\textwidth}
        \includegraphics[width=\textwidth]{figs/pfa_vector}
        \caption{Chroma profiles across edges. No PFA (left). PFA Event (right).}
        \label{sfig:xyY_profiles}
    \end{subfigure}
    \caption{Diagrams in the xyY color space. (a) shows the chromaticity (x-y) space at a fixed luminance (Y). (b) shows two diagrams of the chromaticity profile across an object edge. The left profile contains no PFA trace. The right profile deviates strongly towards 450nm, as depicted by the purple arrow, indicating a strong PFA trace.}
\end{figure}

These strength values are then normalized to be between 0 and 1
\begin{equation}
\delta = \left( \frac{s-s_{min}}{s_{max}-s_{min}} \right)
\end{equation}
where $\delta$ is the normalized PFA strength value, $s_{min}$ is the smallest PFA strength observed in the whole image, and $s_{max}$ is the largest.

\subsubsection{Assigning Reliability Measure}
PFA events that occur at edges of greater contrast and near the image periphery are more distinct and less subject to noise. For each PFA event, a measure of reliability $\rho$ is determined such that
%
\begin{equation}
\rho = \left( \frac{t-t_{min}}{t_{max}-t_{min}} \right)
\left( \frac{d-d_{min}}{d_{max}-d_{min}} \right)
\end{equation}
%
where $t$ is a measure of contrast defined as the absolute difference in pixel intensity across the edge, with $t_{min}$ and $t_{max}$ the smallest and largest observed contrasts in the image. The variable $d$ is the linear distance between the PFA event and the image center, with $d_{min}$ and $d_{max}$ the smallest and largest distances observed in the image. 
\subsubsection{Calculating the Location of the Image Center}

The image center is determined from observations of PFA events by using their associated directions $\vec{N}$, strengths $\delta$, and reliability measures $\rho$. To do this, a function is defined that calculates a measure of distance $D_i(x,y)$ of the ith PFA event to a possible image center $(x,y)$, such that
%
\begin{equation}
D_i(x,y) = \frac{(a_ix + b_iy + c_i)^2}{(1-\delta_i\rho_i + \epsilon)^2} + R_i(x,y).
\label{eq:pfa_dist}
\end{equation}
%
The values $a_i$, $b_i$ and $c_i$ form an equation of a line along the PFA direction $\vec{N}$ for the ith PFA event. The numerator of \eqref{eq:pfa_dist} will be zero for any $(x,y)$ that lies along $\vec{N}$ and will grow larger as $(x,y)$ moves away from that line. The values $\delta_i$ and $\rho_i$ are the strength and reliability measure of the ith PFA event, with $\epsilon$ a small constant used to prevent singularities. PFA events with high reliability and strength will have a small denominator, giving higher weight to these PFA events. Finally, the penalty term $R_i(x,y)$ ensures that the center is on the ``correct side" of the PFA event, i.e. that the PFA event is on the distal side of $(x,y)$ and not proximal. The penalty term $R_i(x,y)$ is defined as 
%
\begin{equation}
R_i(x,y) = \left\lbrace \begin{matrix}
\alpha K_i, & \vec{N}_i \boldsymbol{\cdot}(X-X_0)<0 \\
0, & otherwise
\end{matrix}
 \right.
\end{equation}
where $K_i$ is a normalized distance from the edge to tested center point and $\alpha$ is a large penalty factor set to $5\times10^5$. The condition $\vec{N}_i \boldsymbol{\cdot}(X-X_0)$ ensures that the penalty is only applied when test center point $X_0$ is in the opposite direction of $\vec{N}_i$ relative to the PFA event.

The image center is chosen to be the location that minimizes the sum of the distances, such that
%
\begin{equation}
X_0 = \arg\min_{x,y} \sum_i D_i(x,y).
\end{equation}
%
This minimization is solved using a coarse-fine search.

Furthermore, to make the estimation robust to the presence of forgeries. The estimation process is iterated, each time removing the PFA events above the 70th distance percentile from the previous estimate.

\subsubsection{Detecting Traces of Forgery}
Image content that has been implanted through a tampering process will have large distances $D(x_0,y_0)$ to the estimated image center $(x_0,y_0)$. To detect forgeries, a heatmap is generated of distances of all PFA events in the image. Regions that have hot-spots of large distances are determined to be tampered.

