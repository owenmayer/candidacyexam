\subsection{FORGERY DETECTION}
\label{sec:forgerydetection}

From \eqref{eq:e_auth} and \eqref{eq:e_forg}, a hypothesis test is formulated to differentiate between authentic and forged image regions. For a particular region of interest within an image, $H_0$ is defined as the hypothesis that the image region is unaltered and $H_1$ as the hypothesis that the region has been falsified through copy-paste forgery.
\begin{equation}
\begin{split}
H_0:\quad &\err \sim \mathcal{N}(\bmu_0,\cov) \\%, \ \forall \i\ s.t.\ \xr_i \in ROI \\
H_1:\quad &\err \sim \mathcal{N}(\bmu_0 + \fo,\cov) %, \ \forall \i\ s.t.\ \xr_i \in ROI \\
\end{split}
\label{eq:hypotheses}
\end{equation}
Here $\bmu_0$ and $\cov$ are the mean and covariance of observational noise in the image.
The following equation specifies the probability density of a sequence \mbox{$\{\err_1, \err_2, \ldots, \err_N\}$} of $N$ independent and identically distributed observations of $m$ dimensional model discrepancy.
\vspace{-1mm}
\begin{equation}
\begin{split}
p(&\err_1, \ldots, \err_N | \bmu, \cov)  \\
&= \frac{ |\cov|^{-N/2}}{2 \pi^{Nm/2}}\exp \left\lbrace -\frac{1}{2}\sum_{i=1}^{N}(\err_i - \bmu)^T \cov^{-1}(\err_i - \bmu)  \right\rbrace
\end{split}
\label{eq:pdfNDGauss}
\end{equation}
%
From the hypotheses in \eqref{eq:hypotheses} and density in \eqref{eq:pdfNDGauss},a log-likelihood ratio test is constrcuted to determine whether a sequence of error observations is inauthentic.
\begin{equation}
\begin{split}
\log&\left(\frac{p(\err_1, \ldots, \err_N | \bmu_0 + \fo, \cov)}{p(\err_1, \ldots, \err_N | \bmu_0, \cov)} \right)  \\
&= -\frac{1}{2}\sum_{i=1}^{N}(\err_i - \bmu_0 - \fo)\trans \cov^{-1}(\err_i - \bmu_0 - \fo) \\
&\quad + \frac{1}{2}\sum_{i=1}^{N}(\err_i - \bmu_0)\trans \cov^{-1}(\err_i - \bmu_0) \\
%&= - \frac{1}{2}\sum_{i=1}^{N}\left(m(\err_i,\bmu_0 + \fo) - m(\err_i, \bmu_0) \right)
\end{split}
\label{eq:forgNeymanPearsonExpanded}
\end{equation}
%
Further algebraic reduction of \eqref{eq:forgNeymanPearsonExpanded} yields a simplified form of the optimal detector:
%
\begin{equation}
\left(\bar{\err}-\bmu_0\right)\trans \cov^{-1} \fo \overset{H_0}{\underset{H_1}\lessgtr} \tau
\label{eq:optimalDecision}
\end{equation}
%
where $\bar{\err}$ is the sample average of $\err_i$, and $\tau$ is a decision threshold.

In most practical forensic scenarios the forgery offset $\fo$ is unknown, since information regarding the source forgery content is not available.
In these scenarios the optimal decision feature \eqref{eq:optimalDecision} cannot be computed explicitly. Instead, I form a maximum-likelihood estimate of the forgery offset $\hat{\fo}$. 
Since error in a forged region is distributed Gaussian with mean $\bmu_0 + \fo$, the forgery offset is estimated by 
\begin{equation}
\vspace{0mm}
\hat{\fo} = \frac{1}{N}\sum_{i=1}^{N}\err_i  -  \bmu_0 = \bar{\err} - \bmu_0
\label{eq:forgOffsetEstimate}
\end{equation}
Substituting $\hat{\fo}$ for $\fo$ in \eqref{eq:optimalDecision} yields:
\begin{equation}
\left(\bar{\err}-\bmu_0\right)\trans \cov^{-1} \left(\bar{\err}-\bmu_0\right) \overset{H_0}{\underset{H_1}\lessgtr} \tau
\label{eq:proposedDetector}
\end{equation}
which is the final form of the detection metric.

Since $\cov$ and $\bmu_0$ are often unknown apriori, they must also be estimated. %We follow the convention set by Johnson and Farid, and assume the forgery be sufficiently small such that it does not introduce bias to global estimates \cite{johnson2006exposing}. 
Estimates of $\cov$ and $\bmu_0$ are made from all model discrepancy observations within an image.