\section{Synthesis}
In this work, I develop a method for detecting cut-paste attacked images using lateral chromatic aberration as an authenticity fingerprint. To do this, I propose a new statistical model that describes the mismatch between local and global LCA displacement estimates in authentic and forged scenarios. I use this model to define a hypothesis testing problem, and derive the optimal detection statistic that distinguishes between authentic and forged image regions.


The effects of LCA are characterized by the mapping of a point $\xr = (r_x, r_y)\trans$ in a reference color channel of an image to the location of the corresponding point $\xc = (c_x, c_y)\trans$ in a comparison color channel.
Johnson and Farid proposed the following parametric model of this mapping 
%
\begin{equation}
\label{eq:JF_LCA_model}
% \xc = f(\xr, \JFp) = \acoef (\xr - \oc) + \oc
\xc = \acoef (\xr - \oc) + \oc
\end{equation}
where $\acoef$ is 
a first order expansion coefficient, and $\oc = (\zeta_x, \zeta_y)\trans$ is the location of the image's optical center. (Note this an equivalent representation of the model \eqref{eq:lca_model_xy}, but with slight notation changes)
%
The effect of LCA at location $\xr$ is also described by a displacement vector $\lcadisp$. The LCA 
\begin{equation}
\lcadisp = \xc - \xr = \acoef (\xr - \oc) + \oc - \xr .
\label{eq:lcadisp}
\end{equation}
An example of %the 
an image's LCA displacement vector field is shown in the left of Fig. {\ref{fig:dCLA_auth}}. For the model \eqref{eq:JF_LCA_model}, LCA displacements point radially outward (inward) from the optical center for expansion coefficients greater (less) than 1.
%

\subsection{Lateral Chromatic Aberration Estimation}
\label{ssec:background-lca_est}
In practice, the LCA model parameters $\acoef$ and $\oc$ are typically unknown and must be estimated. Gloe et al. provide a technique to estimate the model parameters~\cite{gloe2010efficient}.
%
Their method operates by obtaining \textit{local} estimates of the LCA displacement vector $\hat{\mathbf{d}}$ at several
corner points located throughout an image. The \textit{global} LCA model parameters are then identified by performing a least-squares fit of the estimated displacement vectors to the model \eqref{eq:lcadisp} using an iterative Gauss-Newton method.
%
\begin{figure}%code for two images side by side
\centering
\begin{minipage}[b]{.35 \linewidth}
\centering
  \includegraphics[width=\linewidth, angle = 0 ]{figs/dLCA_auth.png} 
\end{minipage}%
\quad
\begin{minipage}[b]{.35\linewidth}
\label{fig:dLCA_falsified}
\centering
  \includegraphics[width=\linewidth, angle =0 ]{figs/dLCA_forg.png} 
\end{minipage}
\caption{\textbf{Left:} Lateral chromatic aberration (LCA) displacement vector field for an authentic image. \textbf{Right:} LCA displacement vector field for a copy-paste forgery, with pasted LCA in red. Displacement vectors are scaled 200X.
}
\label{fig:dCLA_auth}
\end{figure}
%
Local estimates of the LCA displacement vector $\hat{\mathbf{d}}$ are obtained by searching for a $W \times W$ block $\mathbf{C}$ in the comparison channel that maximizes similarity with an equivalently sized block $\mathbf{R}$ in the reference channel, such that
\begin{equation}
\label{eq:localLCAest}
\hat{\mathbf{d}} = \underset{(m,n)\in \left\{-\Delta,\ldots,\Delta\right\}}{\arg \max} 
S(\mathbf{R}(x,y),\mathbf{C}(x+m,y+n))
\end{equation}
where the similarity $S(\cdot)$ is measured using the correlation coefficient, and $(x,y)$ denote the horizontal and vertical location of a corner point, about which the blocks are centered.
To enable a search over fractional pixel displacements, both blocks are upsampled by a factor of $u$. This search is performed exhaustively over a set of displacements $(m,n)$ between $-\Delta$ and $\Delta$.

\input{Sections/app_ErrorModel}
\input{Sections/app_forgerydetection}
\input{Sections/app_experimentalresults}