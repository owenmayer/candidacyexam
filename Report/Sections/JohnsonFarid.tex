\section{Exposing Digital Forgeries through Chromatic Aberration}

The authors Johnson and Farid, in their work titled \textit{Exposing Digital Forgeries through Chromatic Aberration} \cite{johnson2006exposing}, present a technique for determining the authenticity of digital images. They do this using an optical fingerprint called chromatic aberration. Chromatic aberration is an optical distortion that is introduced into images by the lens system. The authors present a technique for measuring and characterizing a specific type of chromatic abberation, called lateral chromatic aberration (LCA). Cut-paste image attacks introduce localized inconsistencies to the image's LCA fingerprint. To expose images that have been attacked by a cut-paste operation, the authors present a method for finding localized anomalies in the image's LCA. %Finally, the authors conduct experiments to validate both their LCA characterization method and to demonstrate efficacy of detecting cut-paste image forgeries.

\subsection{Model of Lateral Chromatic Aberration (LCA)}

When taking a digital photograph, light from a scene is focused by a lens onto the imaging sensor through refraction. The refractive index of glass, however, is dependent on the wavelength of light passing through it. The relationship between angle of incidence, angle of refraction, and refractive indices are described by Snell's law,
%
\begin{equation}
n\sin\left(\theta\right) = n_f\sin\left(\theta_f\right)
\end{equation}
%
where $\theta$ is the angle of incidence, $\theta_f$ is the angle of refraction, $n$ is the refractive index of the medium through which light travels (typically air), and $n_f$ is the refractive index of the lens medium (typically glass, or plastic in cell phones).
Solving for the angle of refraction yields:
%
\begin{equation}
\theta_f = \sin^{-1}\left(\frac{n}{n_f}\sin\left(\theta\right)\right)
\end{equation}
%
which shows that the angle of refraction depends on the ratio of refractive indices, which in turn are dependent on the wavelength of light that is being focused. 

Since digital images capture polychromatic light, the different wavelengths of a single light ray are then refracted at different angles, according to their wavelength, and intersect the imaging sensor at different locations. This effect is seen in Fig. \ref{sfig:LCA1D}, which shows a ray-tracing diagram of a lens focusing a polychromatic light ray onto an imaging sensor. The red and blue components are refracted at angles $\theta_r$ and $\theta_b$, respectively, and are focused onto the imaging sensor at locations $x_r$ and $x_b$. The blue and red focal locations $x_r$ and $x_b$ are laterally offset from each other, even though they were reflected from the same point source in the scene. 

This phenomenon is known as lateral chromatic aberration (LCA). An example of how LCA manifests in an image is shown in the appendix in Fig. \ref{fig:lca_irl}. In this example LCA appears as red and blue fringes around objects of an image.
\begin{figure}
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/lca1d.png}
        \caption{LCA in 1D}
        \label{sfig:LCA1D}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figs/lca2d.png}
        \caption{LCA in 2D}
        \label{sfig:LCA2D}
    \end{subfigure}
    \caption{Ray tracing diagrams depicting lateral chromatic aberration (LCA) in a) 1D and b) 2D. Due to Snell's law, different color components of polychromatic light rays are focused on laterally offset sensor locations. The displacement vectors between focal points forms a unique fingerprint that is exploited for forgery detection.}\label{fig:LCARayTrace}
\end{figure}


Johnson and Farid show that the focal point locations of one color channel to be approximately expanded or contracted relative to the focal point locations of another color channel, about the optical center of the image. This is made explicit by the following:
%
\begin{align}
x_r &= \alpha(x_b - x_0) + x_0\\
y_r &= \alpha(y_b - y_0) + y_0
\label{eq:lca_model_xy}
\end{align}
%
where $(x_r,y_r)$ is a point in the red color channel, $(x_b,y_b)$ is its corresponding location in the blue color channel, $(x_0,y_0)$ is the optical center of the image, and $\alpha$ is the expansion coefficient between the blue and red color channels. That is, the red channel focal point locations are expanded/contracted by coefficient $\alpha$ relative to corresponding blue focal point locations, about optical center $(x_0,y_0)$. This can be generalized to any pairing of color channels (red, green, or blue), but with different optical centers or expansion coefficients for each channel pairing.

Furthermore, a vector description of LCA is made that describes the displacement between corresponding focal point locations. The vector
%
\begin{equation}
\mathbf{v} = \begin{pmatrix}
x_r-x_b \\
y_r-y_b
\end{pmatrix}
\label{eq:lca_disp1}
\end{equation}
%
describes the displacement between a focal point in the red color channel $(x_r,y_r)$ and corresponding focal point in the blue color channel $(x_b,y_b)$. The model of \eqref{eq:lca_model_xy} can be substituted into \eqref{eq:lca_disp1} to form a parametric form of displacement
%
\begin{equation}
\mathbf{v}(x,y) = \begin{pmatrix}
\alpha\left(x-x_0\right) - x + x_0 \\
\alpha\left(y-y_0\right) - y + y_0
\end{pmatrix}
\label{eq:lca_disp2}
\end{equation}
%
where $(x,y)$ is a point in a arbitrary reference color channel, $(x_0,y_0)$ is the optical center of that channel, and $\alpha$ is the LCA expansion coefficient between the reference channel and another color channel. Due to the expansion model of LCA, displacements form a distinct pattern that point radially inward or outward away from the image optical center $(x_0,y_0)$. This can be seen in Fig. \ref{sfig:LCA2D}, which shows LCA displacements between the red and blue channel of an image.

\begin{figure}
\centering
\includegraphics[width=.5\linewidth]{figs/lca_example_irl.pdf}
\caption{An example of lateral chromatic aberration in an image. The red and blue LCA “fringes” become visible in the right inset, which is scaled 50 times larger than the original. This unaltered, JPEG compressed image was taken by a Canon Powershot ELPH 160 camera.}
\label{fig:lca_irl}
\end{figure}
\subsection{Estimating the Chromatic Aberration Model}

Lateral chromatic aberrations between two color channels are characterized by the tuple $\boldsymbol{\theta} = \left(x_0,y_0,\alpha\right)$ comprised of the optical center $\left(x_0,y_0\right)$ and expansion coefficient $\alpha$. Johsnon and Farid propose a method to estimate the LCA model parameter tuple in an image. To do this, they find the tuple that maximizes the mutual information between two color channels, where one channel channel is a reference channel and the other channel is corrected by the parameter tuple
%
\begin{equation}
\boldsymbol{\theta}^* = \arg\max_{\boldsymbol{\theta}}I(\mathcal{R};\mathcal{G})
\label{eq:lca_paramtermax}
\end{equation}
where $\mathcal{R}$ and $\mathcal{G}$ are random variables from which the pixel intensities of the corrected red channel and reference green channel are drawn.  The mutual information is defined as
%
\begin{equation}
I(\mathcal{R};\mathcal{G}) = \sum_{r\in\mathcal{R}}\sum_{g\in\mathcal{G}}P(r,g)
\log\left(\frac{P(r,g)}{P(r)P(g)}\right)
\end{equation}
%
where $P(\cdot,\cdot)$ is the joint probability distribution of pixel intensities, and $P(\cdot)$ is the marginal distribution of pixel intensities. These distributions are determined empirically from the image itself. To solve the maximization in \eqref{eq:lca_paramtermax} the authors employ a coarse-fine search of the parameter space. 

To summarize, Johnson and Farid's method corrects the LCA of one color channel, via interpolation according to parameters $\boldsymbol{\theta}$, and then choose the parameters that maximize the similarity (via mutual information) between the corrected channel and a reference channel. This provides a model of chromatic aberration between two color channels. Johnson and Farid perform this estimation for the Green and Red color channel pairs, as shown above, as well as the Green and Blue color channel pairs.


\subsection{Forgery Detection}

Johnson and Farid propose a method to use lateral chromatic aberration to detect image forgeries. When image content is cut and pasted into another image, the LCA inherent to the cut region is transferred to the paste region. This is made apparent by the diagrams in Fig. \ref{fig:lca_fieldforgery_example}, which shows the LCA displacement vector fields for both an unaltered and forged image.

\begin{figure}
\centering
\includegraphics[page=12,width=.6\linewidth]{figs/ppt_figs.pdf}
\caption{Source (top left) and Forged (top right) images, with their corresponding LCA displacement fields (bottom). In forged images, the LCA from the cut region is transferred to the paste region. This creates a localized inconsistency in the LCA fingerprint.}
\label{fig:lca_fieldforgery_example}
\end{figure}

To detect forged regions, Johnson and Farid estimate the LCA parameter tuple in a local image patch $\boldsymbol{\theta}_{loc} = \left(x_{0,loc},y_{0,loc},\alpha_{loc}\right)$ and determine the displacement vector for each point $(x,y)$ in the local patch:
%
\begin{equation}
\mathbf{v}_{loc}(x,y) = \begin{pmatrix}
\alpha_{loc}\left(x-x_{0,loc}\right) - x + x_{0,loc} \\
\alpha_{loc}\left(y-y_{0,loc}\right) - y + y_{0,loc}
\end{pmatrix}
\label{eq:lca_disp_local}
\end{equation}
%
The local displacement vectors are then compared to the LCA displacement vectors predicted by the global parameter tuple $\boldsymbol{\theta}_{gbl} = \left(x_{0,gbl},y_{0,gbl},\alpha_{gbl}\right)$, which is estimated from the entire image, described as follows:
%
\begin{equation}
\mathbf{v}_{gbl}(x,y) = \begin{pmatrix}
\alpha_{gbl}\left(x-x_{0,gbl}\right) - x + x_{0,gbl} \\
\alpha_{gbl}\left(y-y_{0,gbl}\right) - y + y_{0,gbl}
\end{pmatrix}
\label{eq:lca_disp_global}
\end{equation}
%
The vectors $\mathbf{v}_{loc}(x,y)$ characterize the displacements local to an image patch, whereas the vectors $\mathbf{v}_{gbl}(x,y)$ describe the displacements that are characteristic of the entire image. In an authentic region the local and global LCA displacement vectors are similar, whereas in a tampered region the local and global LCA displacement vectors are significantly different.

To quantify the error between the locally estimate and globally estimate LCA, Johnson and Farid propose taking the angle between the local and global displacement vectors. The angular error $\phi(x,y)$ is defined as
\begin{equation}
\phi(x,y) = \cos^{-1}\left(\frac{
\mathbf{v}_{gbl}(x,y)\cdot\mathbf{v}_{loc}(x,y)}{
||\mathbf{v}_{gbl}(x,y)||~||\mathbf{v}_{loc}(x,y)||}
\right)
\end{equation}
%
Then, the average angular error is is determined over all pixels in the region of interest. The average angular error $\bar{\phi}$ is define as:
\begin{equation}
\bar{\phi} = \frac{1}{N}\sum_{(x,y)\in ROI}\phi(x,y)
\end{equation}
where $ROI$ is a region of interest that contains $N$ pixels. If $\bar{\phi}$ is above a certain threshold, then the region of interest is declared to have been falsified through a cut-paste forgery attack. To improve accuracy, Johnson and Farid only consider vectors whose norms are larger than 0.01 pixels when calculating the average angular error. 

This method of forgery detection assumes that the forged region is sufficiently small so that the global estimate of LCA $\boldsymbol{\theta}_{gbl}$ is not biased by the local deviations introduced by the forged content.

Fig. \ref{fig:lca_err_hist} shows a histogram of the average angular error in authentic image patches. The authors found that most average angular error is less than $60\deg$ in authentic patches. They use this angle as a threshold for forgery detection. That is, any patch with an average angular error greater than $60\deg$ is considered forged. Fig. \ref{fig:lca_forgDetec_example} shows an original and tampered image. The image patches overlaid in red have been identified by the Johnson and Farid algorithm as falsified.
%\subsection{Key Findings}

%The authors conduct an experiment to determine the average angular error in authentic image patches. To do this, they start with 205 unaltered images and select 50 blocks of size $300\times300$ pixels in each image. The blocks are chosen to be those with the highest average gradients, since pixel variance is required to get a meaningful similarity measurement during the LCA estimation step. 
%
%The average angular error $\bar{\phi}$ is determined for each block, and the histogram of average errors is shown in Fig. \ref{fig:lca_err_hist}. The authors find that the average $\bar{\phi}$ is 14.8 degrees, with 98.0\% of patches having a $\bar{\phi}$ less than 60 degrees.
%
%The authors then use 60 degrees as a threshold to test cut-paste attacked images. The authors create a series of cut-paste attacked images, and show that their proposed methodology can effectively detect the tampered regions. Such an example is shown in Fig. \ref{fig:lca_forgDetec_example}


\begin{figure}
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{figs/lca_errHist_auth}
        \caption{$\bar{\phi}$ histogram in authentic patches}
        \label{fig:lca_err_hist}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.65\textwidth}
        \includegraphics[width=\textwidth]{figs/lca_forgeryDetection_example}
        \caption{Original and cut-paste attacked image with forgery detection analysis overlaid}
        \label{fig:lca_forgDetec_example}
    \end{subfigure}
    \caption{Left: A histogram of average angular error of LCA displacement in authentic images. Right: An original and forged image. The patches overlaid in red have been detected as falsified by Johnson and Farid's forgery detection algorithm.}
\end{figure}
